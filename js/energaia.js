
/* ---------------------------------------------------------------------
                      COUNTDOWN
---------------------------------------------------------------------- */
// Set the date we're counting down to
var countDownDate = new Date("Dec 9, 2020 15:37:25").getTime();
var x = setInterval(function() {
  // Get today's date and time
  var now = new Date().getTime();
  // Find the distance between now and the count down date
  var distance = countDownDate - now;
  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  // Display the result in the element with id="countdown"
  document.getElementById("countdown").innerHTML = "OUVERTURE" + "<br>"+ "J-" + days ;
  // If the count down is finished, write some text
  if (distance < 0) {
    clearInterval(x);
	$("#countdown").html("RENDEZ-VOUS L'ANNÉE PROCHAINE");
  }
}, 1000 );
/* ---------------------------------------------------------------------
                      SIDEBAR ANIMATION
---------------------------------------------------------------------- */

 $(function() {
	// Get the navbar
  var sidebar = $("#sidebar");
  // When the user scrolls the page, execute myFunction
  $(window).scroll(function() {
	  var scroll = $(window).scrollTop();
	 // if we've scrolled down, change its position to fixed to stick to top 
	 // with adding the CSS properties "sticky" to the sidebar,
	 // otherwise change it back to relative
      if (scroll >= 100) {
        sidebar.addClass("sticky");
      } else {
        sidebar.removeClass("sticky").addClass('remove');
      }
  });
});


 
/////////////////////////////////////////////////// LES FONCTIONS ///////////////////////////////////////////////////	

/**
 * Convert specials HTML entities HTML in character
 */
function htmlspecialchars_decode(str) 
{
	if (typeof(str) == "string")
	{
		str = str.replace(/&amp;/g, "&");
		str = str.replace(/&quot;/g, "\"");
		str = str.replace(/&#039;/g, "'");
		str = str.replace(/&lt;/g, "<");
		str = str.replace(/&gt;/g, ">");
	}
	return str;
}
/**
 * Au chargement de la page ce datatable est affiché 
 * When the page loads this datatable is displayed
 */
aOfPersonnes = [];
function loadAdm_admin()	
{
	var datas = 
	{
		page : "liste_adm_admin",
		bJSON : 1
	}
	$.ajax(
	{
		type: "POST",
		url: "route.php",
		async: true,
		data: datas,
		dataType: "json",
		cache: false,
	})
	/* Si toutes les parties(html,js,php,sql) communiquent comme prévu le .done est lancé sinon c'est le .fail
	   If all the parts (html,js,php,sql) communicate as expected the .done is launched if not it's the .fail
	*/
	.done(function(result) 
	{
		var iAdm_admin= 0;
		for (var ligne in result)	
		{
			aOfPersonnes[iAdm_admin]= [];
			aOfPersonnes[iAdm_admin]["id_user"]= result[ligne]["id_user"];
			aOfPersonnes[iAdm_admin]["id_center"]= result[ligne]["id_center"];
			aOfPersonnes[iAdm_admin]["email_user"]= result[ligne]["email_user"];
			aOfPersonnes[iAdm_admin]["pwd_user"]= result[ligne]["pwd_user"];
			aOfPersonnes[iAdm_admin]["token_user"]= result[ligne]["token_user"];
			aOfPersonnes[iAdm_admin]["reset_token_user"]= result[ligne]["reset_token_user"];
			aOfPersonnes[iAdm_admin]["firstname_user"]= htmlspecialchars_decode(result[ligne]["firstname_user"]);
			aOfPersonnes[iAdm_admin]["lastname_user"]= result[ligne]["lastname_user"];
			aOfPersonnes[iAdm_admin]["tel_user"]= result[ligne]["tel_user"];
			aOfPersonnes[iAdm_admin]["role_user"]= result[ligne]["role_user"];
			aOfPersonnes[iAdm_admin]["active_user"]= result[ligne]["active_user"];
			aOfPersonnes[iAdm_admin]["comment_user"]= result[ligne]["comment_user"];
			aOfPersonnes[iAdm_admin]["date_creation_user"]= result[ligne]["date_creation_user"];
			aOfPersonnes[iAdm_admin]["date_last_connection_user"]= result[ligne]["date_last_connection_user"];
			iAdm_admin++;
		}
	constructTable();
	tables = $('#table_personnes').DataTable(configuration);
	
	})
	.fail(function(err) 
	{
	   alert('error : ' + err.status);
	});
}

function constructTable()	
{
	var i;
	var sHTML= "";
	sHTML+= "<thead class = \" text-center theadMain\">";
	sHTML+= "<tr>";
	sHTML+= "<td>ID</td>";
	sHTML+= "<td>ID centre</td>";
	sHTML+= "<td>Email</td>";
	sHTML+= "<td>PWD</td>";
	sHTML+= "<td>Token user</td>";
	sHTML+= "<td> Reset Token user</td>";
	sHTML+= "<td>Prénom</td>";
	sHTML+= "<td>Nom</td>";
	sHTML+= "<td>Téléphone</td>";
	sHTML+= "<td>Éditer</td>";
	sHTML+= "<td>Supprimer</td>";
	sHTML+= "<td>Role user</td>";
	sHTML+= "<td>Active user</td>";
	sHTML+= "<td>Comment user</td>";
	sHTML+= "<td>Date creation user</td>";
	sHTML+= "<td>Date derniere connection</td>";
	sHTML+= "</tr>";
	sHTML+= "</thead>";
	sHTML+= "<tbody class = text-center>";

	for (i=0; i<aOfPersonnes.length; i++)	
	{
		sHTML+= "<tr>";
		sHTML+= "<td>" + aOfPersonnes[i]["id_user"] + "</td>";
		sHTML+= "<td>" + aOfPersonnes[i]["id_center"] + "</td>";
		sHTML+= "<td>" + aOfPersonnes[i]["email_user"] + "</td>";
		sHTML+= "<td>" + aOfPersonnes[i]["pwd_user"] + "</td>";
		sHTML+= "<td>" + aOfPersonnes[i]["token_user"] + "</td>";
		sHTML+= "<td>" + aOfPersonnes[i]["reset_token_user"] + "</td>";
		sHTML+= "<td>" + aOfPersonnes[i]["firstname_user"] + "</td>";
		sHTML+= "<td>" + aOfPersonnes[i]["lastname_user"] + "</td>";
		sHTML+= "<td>" + aOfPersonnes[i]["tel_user"] + "</td>";
		sHTML+= "<td> <img src= \"assets/img/edit.png\" onClick=\"editPersonne(" + i + ")\"data-toggle=\"modal\" data-target=\"#modal_edition\"></td>";
		sHTML+= "<td> <img src= \"assets/img/trash.png\" onClick=\" recup(" + i + ")\"data-toggle=\"modal\" data-target=\"#infos\"> </td>";
		sHTML+= "<td>" + aOfPersonnes[i]["role_user"] + "</td>";
		sHTML+= "<td>" + aOfPersonnes[i]["active_user"] + "</td>";
		sHTML+= "<td>" + aOfPersonnes[i]["comment_user"] + "</td>";
		sHTML+= "<td>" + aOfPersonnes[i]["date_creation_user"] + "</td>";
		sHTML+= "<td>" + aOfPersonnes[i]["date_last_connection_user"] + "</td>";
		sHTML+= "</tr>";
	}
	sHTML+= "</tbody>";
	$('#table_personnes').html(sHTML);
}

/**fonction qui vérifie que les inputs soient biens remplis
 **function that verifies that the inputs are filled
 **si un seul des champs est vide le modal s'active rajout de la class modal au bouton ajouter si inputs vide 
 **if one of the inputs is empty the modal is activated */
function verif() 
{     
	if ($('#prenom').val() == "" || $('#nom').val() == "" || $('#telephone').val() == "" || $('#email').val() == "" )
	{
		$("#btn_ajouter_modal").attr("data-toggle","modal");
		$("#btn_ajouter_modal").attr("data-target","#infos2");
		return true;
	}
	else 
	{   
		///supprime la classe modal si tout les inputs sont remplis
		//delete modal class if inputs are all filled 
		$("#btn_ajouter_modal").removeAttr("data-toggle","modal");
		$("#btn_ajouter_modal").removeAttr("data-target","#infos2");
		return false;
	}
}	

function ajoutPersonne()
//verif() retourne faux car tout les inputs sont remplis donc la fonction ajoutpersonne se déclenche
//verif() return false if all the inputs are filled and the function ajoutpersonne is activated
{ 
	if(verif() == false)
	{
		var datas = 
		{
			page : "save_adm_admin",
			bJSON : 1, 
			id_center: $('#center').val(),
			email_user: $('#email').val(),
			pwd_user: $('#pwd').val(),
			firstname_user: $('#prenom').val(),
			lastname_user: $('#nom').val(),
			tel_user: $('#telephone').val(),
			comment_user: $('#comment_user').val(),
			date_last_connection_user:$('#date_last').val(),
		}
		$.ajax(
		{
			type: "POST",
			url: "route.php",
			async: true,
			data: datas,
			dataType: "json",
			cache: false,
		})
		.done(function(result) 
		{
			let data = result.last_admin[0];
			var iLongueur= aOfPersonnes.length;
			aOfPersonnes[iLongueur]= [];
			aOfPersonnes[iLongueur]["id_user"]= data.id_user;
			aOfPersonnes[iLongueur]["id_center"]= $('#center').val();
			aOfPersonnes[iLongueur]["email_user"]= $('#email').val();
			aOfPersonnes[iLongueur]["pwd_user"]= $('#pwd').val();
			aOfPersonnes[iLongueur]["firstname_user"]= $('#prenom').val();
			aOfPersonnes[iLongueur]["lastname_user"]= $('#nom').val();
			aOfPersonnes[iLongueur]["tel_user"]= $('#telephone').val();
			aOfPersonnes[iLongueur]["comment_user"]= $('#comment_user').val();
			aOfPersonnes[iLongueur]["date_last_connection_user"]= $('#date_last').val();
			tables.clear();
			tables.destroy();
			constructTable();
			tables = $('#table_personnes').DataTable(configuration);
			
		})
		.fail(function(err) 
		{
			console.log('error : ', err );
			
		});
	}
}
function recup(value) 
{
	iIndiceEditionEncours = value;
}
function supprimPersonne()	
{
	var datas = 
	{
		page : "supprime_adm_admin",
		bJSON : 1, 
		id_user: aOfPersonnes[iIndiceEditionEncours]["id_user"]
	}
	$.ajax(
	{
		type: "POST",
		url: "route.php",
		async: true,
		data: datas,
		dataType: "json",
		cache: false,
	})
	.done(function(result) 
	{	
		aOfPersonnes.splice(iIndiceEditionEncours,1);
		tables.clear();
		tables.destroy();
		constructTable();
		tables = $('#table_personnes').DataTable(configuration);
		
	})
	.fail(function(err) {
		alert("Erreur lors de la suppression de votre adm_admin. Vous allez être déconnecté.");
		console.log('error : ', err );
	});
}

let iIndiceEdit;
function editPersonne(iIndiceEdit)	
////ajout du contenu du tableau dans les inputs du modal_edition
// add content of the table in the inputs of the modal_edition
{
	iIndiceEditionEncours = iIndiceEdit;
	$('#id_user_edit').val(aOfPersonnes[iIndiceEdit]["id_user"]);
	$('#center_edit').val(aOfPersonnes[iIndiceEdit]["id_center"]);
	$('#email_edit').val( aOfPersonnes[iIndiceEdit]["email_user"]);
	$('#pwd_edit').val(aOfPersonnes[iIndiceEdit]["pwd_user"]);
	$('#token_edit').val(aOfPersonnes[iIndiceEdit]["token_user"]);
	$('#reset_token_edit').val(aOfPersonnes[iIndiceEdit]["reset_token_user"]);
	$('#prenom_edit').val( aOfPersonnes[iIndiceEdit]["firstname_user"]);
	$('#nom_edit').val( aOfPersonnes[iIndiceEdit]["lastname_user"]);
	$('#telephone_edit').val( aOfPersonnes[iIndiceEdit]["tel_user"]);
	$('#role_user_edit').val( aOfPersonnes[iIndiceEdit]["role_user"]);
	$('#active_user_edit').val( aOfPersonnes[iIndiceEdit]["active_user"]);
	$('#comment_user_edit').val( aOfPersonnes[iIndiceEdit]["comment_user"]);
	$('#date_creation_edit').val( aOfPersonnes[iIndiceEdit]["date_creation_user"]);
	$('#date_last_edit').val( aOfPersonnes[iIndiceEdit]["date_last_connection_user"]);
}
let iIndiceEditionEncours;
function majPersonne()	
{
	var datas = 
	{ 
		page : "update_adm_admin",
		bJSON : 1,
		id_user:$('#id_user_edit').val(),
		id_center: $('#center_edit').val(),
		email_user: $('#email_edit').val(),
		pwd_user: $('#pwd_edit').val(),
		token_user: $('#token_edit').val(),
		reset_token_user: $('#reset_token_edit').val(),
		firstname_user: $('#prenom_edit').val(),
		lastname_user: $('#nom_edit').val(),
		tel_user: $('#telephone_edit').val(),
		role_user:$('#role_user_edit').val(),
		active_user: $('#active_user_edit').val(),
		comment_user: $('#comment_user_edit').val(),
		date_creation_user: $('#date_creation_edit').val(),
		date_last_connection_user:$('#date_last_edit').val(),
	}

	$.ajax({
		type: "POST",
		url: "route.php",
		async: true,
		data: datas,
		dataType: "json",
		cache: false,
	})

	.done(function(result) {
		console.log("result", result)
		aOfPersonnes[iIndiceEditionEncours]["id_user"]= $('#id_user_edit').val();
		aOfPersonnes[iIndiceEditionEncours]["id_center"]= $('#center_edit').val();
		aOfPersonnes[iIndiceEditionEncours]["email"]= $('#email_edit').val();
		aOfPersonnes[iIndiceEditionEncours]["pwd"]= $('#pwd_edit').val();
		aOfPersonnes[iIndiceEditionEncours]["token_user"]= $('#token_edit').val();
		aOfPersonnes[iIndiceEditionEncours]["reset_token_user"]= $('#reset_token_edit').val();
		aOfPersonnes[iIndiceEditionEncours]["firstname_user"]= $('#prenom_edit').val();
		aOfPersonnes[iIndiceEditionEncours]["lastname_user"]= $('#nom_edit').val();
		aOfPersonnes[iIndiceEditionEncours]["tel_user"]= $('#telephone_edit').val();
		aOfPersonnes[iIndiceEditionEncours]["role_user"]= $('#role_user_edit').val();
		aOfPersonnes[iIndiceEditionEncours]["active_user"]= $('#active_user_edit').val();
		aOfPersonnes[iIndiceEditionEncours]["comment_user"]= $('#comment_user_edit').val();
		aOfPersonnes[iIndiceEditionEncours]["date_creation_user"]= $('#date_creation_edit').val();
		aOfPersonnes[iIndiceEditionEncours]["date_last_connection_user"]= $('#date_last_edit').val();
		loadAdm_admin();
		
		} 
	)
	.fail(function(err) {
		console.log('error : ', err);
	});
}
$(document).ready(function() {
	loadAdm_admin();
});


/** 
 * Configuration du datatable 
 **/
const configuration = {
	"stateSave": false,
	"order": [[1, "asc"]],
	"pagingType": "simple_numbers",
	"searching": true,
	"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Tous"]], 
	"language": {
		"info": "Utilisateurs _START_ à _END_ sur _TOTAL_ sélectionnées",
		"emptyTable": "Aucun utilisateur",
		"lengthMenu": "_MENU_ Utilisateurs par page",
		"search": "Rechercher : ",
		"zeroRecords": "Aucun résultat de recherche",
		"paginate": {
			"previous": "Précédent",
			"next": "Suivant"
		},
		"sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
		"sInfoEmpty":      "Utilisateurs 0 à 0 sur 0 sélectionnée",
	},
	
	"columns": [
		{
			"orderable": true
		},
		{
			"orderable": true
		},
		{
			"orderable": true
		},
		{
			"orderable": true
		},
		{
			"orderable": true
		},
		{
			"orderable": true
		},
		{
			"orderable": true
		},
		{
			"orderable": true
		},
		{
			"orderable": true
		},
		{
			"orderable": true
		},
		{
			"orderable": true
		},
		{
			"orderable": true
		},
		{
			"orderable": true
		},
		{
			"orderable": true
		},
		{
			"orderable": false
		},
		{
			"orderable": false
		},
	],
	'retrieve': true
};


			

			
	